plugins {
    id("com.android.application")
    kotlin("android")
    id("com.google.devtools.ksp")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("com.google.firebase.firebase-perf")
    id("androidx.navigation.safeargs.kotlin")
}

android {
    compileSdk = 35
    namespace = "org.kalabovi.cteni"

    defaultConfig {
        applicationId = "org.kalabovi.cteni"
        minSdk = 24
        targetSdk = 35
        versionCode = 23
        versionName = "1.19.0"
    }

    buildFeatures {
        buildConfig = true
        viewBinding = true
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            isShrinkResources = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
        freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
    }
    testOptions.unitTests.all { it.useJUnitPlatform() }
    packagingOptions.resources.excludes += "DebugProbesKt.bin"
    androidResources.generateLocaleConfig = true
}

ksp {
    arg("room.schemaLocation", "$projectDir/schemas")
}

kotlin {
    jvmToolchain(21)
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.10.1")

    implementation("androidx.core:core-ktx:1.16.0-alpha01")
    implementation("androidx.appcompat:appcompat:1.7.0")
    implementation("androidx.fragment:fragment-ktx:1.8.5")
    implementation("androidx.activity:activity-ktx:1.10.0")
    implementation("androidx.datastore:datastore-preferences:1.1.2")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.9.0-alpha08")
    implementation("androidx.recyclerview:recyclerview:1.4.0")

    implementation("com.google.android.material:material:1.13.0-alpha09")
    implementation("androidx.constraintlayout:constraintlayout:2.2.0")

    implementation("com.google.android.play:app-update-ktx:2.1.0")
    implementation("com.google.android.play:review-ktx:2.0.2")

    implementation("com.jakewharton.timber:timber:5.0.1")

    val leakcanaryVersion = "3.0-alpha-8"
    debugImplementation("com.squareup.leakcanary:leakcanary-android:$leakcanaryVersion")
    implementation("com.squareup.leakcanary:plumber-android:$leakcanaryVersion")

    implementation("io.insert-koin:koin-android:4.1.0-Beta5")

    val roomVersion = "2.7.0-alpha12"
    implementation("androidx.room:room-ktx:$roomVersion")
    ksp("androidx.room:room-compiler:$roomVersion")

    implementation(platform("com.google.firebase:firebase-bom:33.8.0"))
    implementation("com.google.firebase:firebase-crashlytics-ktx")
    implementation("com.google.firebase:firebase-perf-ktx")

    val navVersion = "2.9.0-alpha04"
    implementation("androidx.navigation:navigation-fragment-ktx:$navVersion")
    implementation("androidx.navigation:navigation-ui-ktx:$navVersion")

    val kotestVersion = "6.0.0.M1"
    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
    val mockkVersion = "1.13.16"
    testImplementation("io.mockk:mockk-android:$mockkVersion")
    testImplementation("io.mockk:mockk-agent:$mockkVersion")
}
