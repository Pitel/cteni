package org.kalabovi.cteni.repo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map
import timber.log.Timber

class ScoreRepository(private val context: Context) {
    val score = context.scoreDataStore.data
        .map { it[SCORE_KEY] ?: 0 }
        .combine(context.scoreDataStore.data.map { it[TOTAL_KEY] ?: 0 }) { score, total ->
            Timber.d("$score/$total")
            score to total
        }

    suspend fun increment(correct: Boolean) {
        context.scoreDataStore.edit {
            Timber.d("incrementScore")
            it[TOTAL_KEY] = (it[TOTAL_KEY] ?: 0).inc()
            if (correct) {
                it[SCORE_KEY] = (it[SCORE_KEY] ?: 0).inc()
            }
        }
    }

    suspend fun reset() {
        context.scoreDataStore.edit {
            Timber.d("resetScore")
            it[TOTAL_KEY] = 0
            it[SCORE_KEY] = 0
        }
    }

    private companion object {
        private val Context.scoreDataStore: DataStore<Preferences> by preferencesDataStore("score")
        private val SCORE_KEY = longPreferencesKey("score")
        private val TOTAL_KEY = longPreferencesKey("total")
    }
}