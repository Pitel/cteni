package org.kalabovi.cteni.repo.db

import android.database.sqlite.SQLiteDatabase
import androidx.core.content.contentValuesOf
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.AutoMigrationSpec
import androidx.room.util.useCursor
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [Letter::class],
    version = 2,
    autoMigrations = [
        AutoMigration(1, 2, ReadingDatabase.CharToStringMigration::class)
    ]
)
abstract class ReadingDatabase : RoomDatabase() {
    abstract fun letterDao(): LetterDao

    class CharToStringMigration : AutoMigrationSpec {
        override fun onPostMigrate(db: SupportSQLiteDatabase) {
            db.query("SELECT letter from Letter").useCursor { cursor ->
                buildList {
                    while (cursor.moveToNext()) {
                        add(cursor.getInt(0).toChar())
                    }
                }
            }.forEach { char ->
                db.update(
                    "Letter",
                    SQLiteDatabase.CONFLICT_REPLACE,
                    contentValuesOf("letter" to "$char"),
                    "letter = ?",
                    arrayOf(char.code)
                )
            }
        }
    }
}