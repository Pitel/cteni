package org.kalabovi.cteni.repo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class ReviewRepository(private val context: Context) {
    suspend fun launches(): Long = context.reviewDataStore.data
        .map { it[LAUNCHES_KEY] ?: 0L }
        .first()

    suspend fun launch() {
        context.reviewDataStore.edit {
            it[LAUNCHES_KEY] = launches() + 1
        }
    }

    private companion object {
        private val Context.reviewDataStore: DataStore<Preferences> by preferencesDataStore("review")
        private val LAUNCHES_KEY = longPreferencesKey("launches")
    }
}
