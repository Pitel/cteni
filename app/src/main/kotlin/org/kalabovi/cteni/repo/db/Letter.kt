package org.kalabovi.cteni.repo.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Letter(
    @PrimaryKey
    val letter: String,
    val index: Int,
    val probability: Float,
    val enabled: Boolean
)
