package org.kalabovi.cteni.repo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import org.kalabovi.cteni.R
import timber.log.Timber

class WordsRepository(private val context: Context) {
    private val words = context.resources.openRawResource(R.raw.words).use {
        it.reader().readLines()
    }

    fun random(letters: CharArray, length: IntRange): String = words
        .filter { it.length in length }
        .filter { it.all { c -> c in letters } }
        .random()

    suspend fun enabled(): Boolean = context.wordsDataStore.data
        .map { it[ENABLED_KEY] ?: true }
        .first()

    suspend fun enabled(enable: Boolean) {
        Timber.d("enabled $enable")
        context.wordsDataStore.edit {
            it[ENABLED_KEY] = enable
        }
    }

    suspend fun length() = context.wordsDataStore.data
        .map { (it[MIN_KEY] ?: 3)..(it[MAX_KEY] ?: 6) }
        .first()

    suspend fun length(length: IntRange) {
        Timber.d("$length")
        context.wordsDataStore.edit {
            it[MIN_KEY] = length.first
            it[MAX_KEY] = length.last
        }
    }

    private companion object {
        private val Context.wordsDataStore: DataStore<Preferences> by preferencesDataStore("words")
        private val ENABLED_KEY = booleanPreferencesKey("enabled")
        private val MIN_KEY = intPreferencesKey("min")
        private val MAX_KEY = intPreferencesKey("max")
    }
}