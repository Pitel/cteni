package org.kalabovi.cteni.repo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import org.kalabovi.cteni.R
import timber.log.Timber

class TtsRepository(private val context: Context) {
    suspend fun enabled(): Boolean = context.ttsDataStore.data
        .map { it[ENABLED_KEY] ?: false }
        .first()

    suspend fun enabled(enable: Boolean) {
        Timber.d("enabled $enable")
        context.ttsDataStore.edit {
            it[ENABLED_KEY] = enable
        }
    }

    private companion object {
        private val Context.ttsDataStore: DataStore<Preferences> by preferencesDataStore("tts")
        private val ENABLED_KEY = booleanPreferencesKey("enabled")
    }
}