package org.kalabovi.cteni.repo.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface LetterDao {
    @Query("SELECT * FROM letter")
    fun getAll(): Flow<List<Letter>>

    @Query("SELECT * FROM letter WHERE enabled = 1")
    fun getEnabled(): Flow<List<Letter>>

    @Update
    fun update(vararg letters: Letter)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg letters: Letter)
}