package org.kalabovi.cteni.repo

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringSetPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import timber.log.Timber

class DrillRepository(private val context: Context) {
    suspend fun isEmpty() = context.drillDataStore.data
        .map { it[DRILL_KEY] ?: emptySet() }
        .map { it.isEmpty() }
        .first()

    suspend fun add(syllable: String) {
        context.drillDataStore.edit {
            it[DRILL_KEY] = (it[DRILL_KEY] ?: emptySet()).toMutableSet().apply { add(syllable) }
        }
        Timber.d("+ $syllable = ${context.drillDataStore.data.map { it[DRILL_KEY] }.first()}")
    }

    suspend fun remove(syllable: String) {
        context.drillDataStore.edit {
            it[DRILL_KEY] = (it[DRILL_KEY] ?: emptySet()).toMutableSet().apply { remove(syllable) }
        }
        Timber.d("- $syllable = ${context.drillDataStore.data.map { it[DRILL_KEY] }.first()}")
    }

    suspend fun get(): String {
        val result = context.drillDataStore.data.map { it[DRILL_KEY] ?: emptySet() }.first().first()
        remove(result)
        Timber.d("Get $result ${context.drillDataStore.data.map { it[DRILL_KEY] }.first()}")
        return result
    }

    private companion object {
        private val Context.drillDataStore: DataStore<Preferences> by preferencesDataStore("drill")
        private val DRILL_KEY = stringSetPreferencesKey("drill")
    }
}