package org.kalabovi.cteni

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.core.graphics.Insets
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.FragmentContainerView
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.AppUpdateOptions
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import org.kalabovi.cteni.App.Companion.INSETS_KEY
import org.kalabovi.cteni.App.Companion.LETTERS
import org.kalabovi.cteni.repo.db.ReadingDatabase
import org.koin.android.ext.android.get
import org.koin.android.ext.android.getKoin
import org.koin.androidx.scope.ScopeActivity
import timber.log.Timber

class MainActivity : ScopeActivity(R.layout.main) {
    private val appUpdateManager by lazy { AppUpdateManagerFactory.create(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        val fragmentContainer = findViewById<FragmentContainerView>(R.id.container)
        ViewCompat.setOnApplyWindowInsetsListener(fragmentContainer) { _, windowInsets ->
            getKoin().getProperty<MutableStateFlow<Insets>>(INSETS_KEY)!!.value =
                windowInsets.getInsets(WindowInsetsCompat.Type.systemBars())
            WindowInsetsCompat.CONSUMED
        }
        lifecycleScope.launch(Dispatchers.IO) {
            get<ReadingDatabase>().letterDao().insert(*LETTERS)
        }
        val navController = fragmentContainer.getFragment<NavHostFragment>().navController
        setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))

        lifecycleScope.launch {
            try {
                val appUpdateInfo = appUpdateManager.appUpdateInfo.await()
                Timber.d("updateAvailability ${appUpdateInfo.updateAvailability()}")
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                    appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                ) {
                    appUpdateManager.startUpdateFlow(
                        appUpdateInfo,
                        this@MainActivity,
                        AppUpdateOptions.defaultOptions(AppUpdateType.IMMEDIATE)
                    )
                }
            } catch (ce: CancellationException) {
                throw ce
            } catch (e: Exception) {
                Timber.w(e)
            }
        }
    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.container).navigateUp() || super.onSupportNavigateUp()

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            try {
                val appUpdateInfo = appUpdateManager.appUpdateInfo.await()
                Timber.d("updateAvailability ${appUpdateInfo.updateAvailability()}")
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    appUpdateManager.startUpdateFlow(
                        appUpdateInfo,
                        this@MainActivity,
                        AppUpdateOptions.defaultOptions(AppUpdateType.IMMEDIATE)
                    )
                }
            } catch (e: Exception) {
                Timber.w(e)
            }
        }
    }
}
