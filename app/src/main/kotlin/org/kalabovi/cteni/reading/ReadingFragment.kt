package org.kalabovi.cteni.reading

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.MarginLayoutParams
import androidx.core.graphics.Insets
import androidx.core.net.toUri
import androidx.core.view.MenuProvider
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.kalabovi.cteni.App.Companion.INSETS_KEY
import org.kalabovi.cteni.R
import org.kalabovi.cteni.databinding.ReadingBinding
import org.koin.android.ext.android.getKoin
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ReadingFragment : ScopeFragment(), MenuProvider {
    private var binding: ReadingBinding? = null

    private val viewModel: ReadingViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ReadingBinding.inflate(layoutInflater, container, false)
        .also {
            binding = it
        }
        .root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getKoin().getProperty<StateFlow<Insets>>(INSETS_KEY)!!.onEach {
            Timber.d("$it")
            view.updateLayoutParams<MarginLayoutParams> {
                topMargin = it.top
                leftMargin = it.left
                bottomMargin = it.bottom
                rightMargin = it.right
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
        viewModel.syllable.onEach {
            binding?.text?.text = it
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        viewModel.score.onEach {
            binding?.score?.text = "${it.first}/${it.second}"
            if (it.second != 0L) {
                binding?.progress?.setProgressCompat(
                    ((it.first.toFloat() / it.second.toFloat()) * (binding?.progress?.max ?: 100)).toInt(),
                    true
                )
            } else {
                binding?.progress?.isIndeterminate = true
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)

        binding?.correct?.setOnClickListener {
            viewModel.viewModelScope.launch {
                viewModel.correct()
            }
        }
        binding?.wrong?.setOnClickListener {
            viewModel.viewModelScope.launch {
                viewModel.wrong()
            }
        }

        requireActivity().addMenuProvider(this, viewLifecycleOwner)
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu, menu)
        viewLifecycleOwner.lifecycleScope.launch {
            menu.findItem(R.id.words).isChecked = viewModel.wordsEnabled()
            menu.findItem(R.id.tts).isChecked = viewModel.ttsEnabled()
        }
    }

    override fun onMenuItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.reset -> {
            viewModel.viewModelScope.launch {
                viewModel.resetScore()
            }
            true
        }

        R.id.probabilities -> {
            findNavController().navigate(ReadingFragmentDirections.probabilities())
            true
        }

        R.id.words -> {
            item.isChecked = !item.isChecked
            viewModel.viewModelScope.launch {
                viewModel.wordsEnabled(item.isChecked)
            }
            true
        }

        R.id.tts -> {
            item.isChecked = !item.isChecked
            viewModel.viewModelScope.launch {
                viewModel.ttsEnabled(item.isChecked)
            }
            true
        }

        R.id.pp -> try {
            startActivity(Intent(Intent.ACTION_VIEW, getString(R.string.pp_url).toUri()))
            true
        } catch (anfe: ActivityNotFoundException) {
            Timber.w(anfe)
            false
        }

        else -> false
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }
}
