package org.kalabovi.cteni.reading

enum class Capitalization {
    LOWERCASE,
    UPPERCASE,
    CAPITALIZE
}