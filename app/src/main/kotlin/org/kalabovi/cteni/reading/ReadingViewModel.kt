package org.kalabovi.cteni.reading

import android.content.Context
import android.speech.tts.TextToSpeech
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import com.google.firebase.perf.trace
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kalabovi.cteni.App
import org.kalabovi.cteni.repo.DrillRepository
import org.kalabovi.cteni.repo.ScoreRepository
import org.kalabovi.cteni.repo.TtsRepository
import org.kalabovi.cteni.repo.WordsRepository
import org.kalabovi.cteni.repo.db.Letter
import org.kalabovi.cteni.repo.db.ReadingDatabase
import timber.log.Timber
import kotlin.random.Random

class ReadingViewModel(
    private val scoreRepository: ScoreRepository,
    private val drillRepository: DrillRepository,
    private val db: ReadingDatabase,
    private val wordsRepository: WordsRepository,
    private val ttsRepository: TtsRepository,
    context: Context
) : ViewModel() {
    private val _syllable = MutableStateFlow("")
    val syllable: StateFlow<String> = _syllable

    val score = scoreRepository.score

    private val ttsTrace = Firebase.performance.newTrace("tts")

    private val tts = viewModelScope.async {
        withContext(Dispatchers.IO) {
            ttsTrace.start()
        }
        TextToSpeech(context) {
            launch {
                Timber.d("TTS $it")
                ttsTrace.putAttribute("result", "$it")
                if (it == TextToSpeech.SUCCESS) {
                    val support = prepareTts()
                    ttsTrace.putAttribute("support", "$support")
                }
                ttsTrace.stop()
            }
        }
    }

    init {
        viewModelScope.launch {
            next()
        }
    }

    private suspend fun prepareTts(): Int = tts.await().setLanguage(App.LOCALE).also {
        Timber.d("TTS support $it")
    }

    suspend fun correct() {
        scoreRepository.increment(true)
        drillRepository.remove(syllable.value)
        next()
    }

    suspend fun wrong() {
        if (ttsEnabled()) {
            val queuing = tts.await().speak(
                syllable.value.lowercase(),
                TextToSpeech.QUEUE_FLUSH,
                null,
                syllable.value
            )
            Timber.d("TTS queuing $queuing")
        }
        scoreRepository.increment(false)
        drillRepository.add(syllable.value)
        next(true)
    }

    private suspend fun next(forceRandom: Boolean = false) {
        withContext(Dispatchers.Default) {
            _syllable.value = Firebase.performance.newTrace("next").trace {
                if (!forceRandom && Random.nextBoolean() && !drillRepository.isEmpty()) {
                    putAttribute(PATH_ATTR, "drill")
                    drillRepository.get()
                } else {
                    if (wordsEnabled() && Random.nextBoolean()) {
                        putAttribute(PATH_ATTR, "word")
                        wordsRepository.random(
                            db.letterDao().getEnabled()
                                .first { it.isNotEmpty() }
                                .joinToString("") { it.letter }
                                .toCharArray(),
                            wordsRepository.length()
                        )
                    } else {
                        putAttribute(PATH_ATTR, "letters")
                        val lettersMap = db.letterDao().getEnabled()
                            .first { it.isNotEmpty() }
                            .groupBy { it.index }
                            .toSortedMap()
                        var result = lettersMap.values.fold("") { acc, letters ->
                            acc + letters.randomChar
                        }
                        while (isActive && result.contains('ě', true) &&
                            result.lowercase() !in arrayOf(
                                "dě",
                                "tě",
                                "ně",
                                "bě",
                                "pě",
                                "vě",
                                "mě",
                            )
                        ) {
                            result = lettersMap.values.fold("") { acc, letters ->
                                acc + letters.randomChar
                            }
                        }
                        result.replace('ú', 'ů')
                    }.let {
                        when (Capitalization.values().random()) {
                            Capitalization.LOWERCASE -> it.lowercase()
                            Capitalization.UPPERCASE -> it.uppercase()
                            Capitalization.CAPITALIZE -> it.lowercase()
                                .replaceFirstChar { c -> c.titlecase() }
                        }
                    }
                }
            }
        }
    }

    suspend fun resetScore() = scoreRepository.reset()

    suspend fun wordsEnabled() = wordsRepository.enabled()

    suspend fun wordsEnabled(enabled: Boolean) = wordsRepository.enabled(enabled)

    suspend fun ttsEnabled() = ttsRepository.enabled()

    suspend fun ttsEnabled(enabled: Boolean) = ttsRepository.enabled(enabled)

    override fun onCleared() {
        viewModelScope.launch {
            tts.await().shutdown()
            super.onCleared()
        }
    }

    companion object {
        private const val PATH_ATTR = "path"

        val Collection<Letter>.randomChar: String
            get() {
                val max = sumOf { it.probability.toDouble() }
                if (max <= 0.0) return random().letter
                val random = Random.nextDouble(max)
                var cumulative = 0.0
                return first {
                    cumulative += it.probability
                    random <= cumulative
                }.letter
            }
    }
}
