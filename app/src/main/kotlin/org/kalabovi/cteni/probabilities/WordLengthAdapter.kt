package org.kalabovi.cteni.probabilities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.google.android.material.slider.RangeSlider
import org.kalabovi.cteni.databinding.WordLengthBinding
import timber.log.Timber

class WordLengthAdapter(
    private val initial: IntRange,
    private val update: (IntRange) -> Unit
) : Adapter<WordLengthAdapter.WordViewHolder>() {

    init {
        setHasStableIds(true)
    }

    override fun getItemCount() = 1

    override fun getItemId(position: Int) = position.toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = WordViewHolder(
        WordLengthBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        with(holder.binding.range) {
            clearOnSliderTouchListeners()
            values = listOf(initial.first.toFloat(), initial.last.toFloat())
            addOnSliderTouchListener(object : RangeSlider.OnSliderTouchListener {
                override fun onStartTrackingTouch(slider: RangeSlider) {}

                override fun onStopTrackingTouch(slider: RangeSlider) {
                    Timber.d("${slider.values}")
                    update(slider.values.first().toInt()..slider.values.last().toInt())
                }
            })
        }
    }

    inner class WordViewHolder(val binding: WordLengthBinding) : ViewHolder(binding.root)
}