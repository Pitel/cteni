package org.kalabovi.cteni.probabilities

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import org.kalabovi.cteni.App
import org.kalabovi.cteni.repo.ReviewRepository
import org.kalabovi.cteni.repo.WordsRepository
import org.kalabovi.cteni.repo.db.ReadingDatabase
import timber.log.Timber
import java.text.Collator

class ProbabilitiesViewModel(
    private val db: ReadingDatabase,
    private val words: WordsRepository,
    private val review: ReviewRepository,
) : ViewModel() {
    val letters = db.letterDao().getAll().map { list ->
        list.sortedWith(compareBy(COLLATOR) { it.letter })
    }

    suspend fun changeProbability(letter: String, probability: Float) {
        withContext(Dispatchers.IO) {
            db.letterDao().update(
                letters.first().first { it.letter == letter }.copy(probability = probability)
            )
        }
    }

    suspend fun changeEnabled(letter: String, enabled: Boolean) {
        Timber.d("$letter $enabled")
        withContext(Dispatchers.IO) {
            db.letterDao().update(
                letters.first().first { it.letter == letter }.copy(enabled = enabled)
            )
        }
    }

    suspend fun wordLength() = words.length()

    suspend fun wordLength(length: IntRange) = words.length(length)

    suspend fun launch() = review.launch()

    suspend fun launches() = review.launches()

    private companion object {
        private val COLLATOR = Collator.getInstance(App.LOCALE)
            .apply { strength = Collator.SECONDARY }
    }
}
