package org.kalabovi.cteni.probabilities

import android.os.Bundle
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import androidx.core.graphics.Insets
import androidx.core.view.updateLayoutParams
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.play.core.review.ReviewManagerFactory
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import org.kalabovi.cteni.App.Companion.INSETS_KEY
import org.kalabovi.cteni.R
import org.koin.android.ext.android.getKoin
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class ProbabilitiesFragment : ScopeFragment(R.layout.probabilities) {
    private val viewModel: ProbabilitiesViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rv = (view as RecyclerView).apply {
            setHasFixedSize(true)
        }
        getKoin().getProperty<StateFlow<Insets>>(INSETS_KEY)!!.onEach {
            Timber.d("$it")
            rv.setPadding(it.left, 0, it.right, it.bottom)
            view.updateLayoutParams<MarginLayoutParams> {
                topMargin = it.top
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
        val probabilitiesAdapter = ProbabilitiesAdapter(
            { letter, probability ->
                viewModel.viewModelScope.launch {
                    viewModel.changeProbability(letter, probability)
                }
            },
            { letter, enabled ->
                viewModel.viewModelScope.launch {
                    viewModel.changeEnabled(letter, enabled)
                }
            }
        )
        viewLifecycleOwner.lifecycleScope.launch {
            rv.adapter = ConcatAdapter(
                ConcatAdapter.Config.Builder()
                    .setStableIdMode(ConcatAdapter.Config.StableIdMode.ISOLATED_STABLE_IDS)
                    .setIsolateViewTypes(true)
                    .build(),
                WordLengthAdapter(viewModel.wordLength()) {
                    Timber.d("$it")
                    viewModel.viewModelScope.launch {
                        Timber.d("$it")
                        viewModel.wordLength(it)
                    }
                },
                probabilitiesAdapter
            )
        }
        viewModel.letters.onEach {
            probabilitiesAdapter.submitList(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
        viewModel.viewModelScope.launch {
            viewModel.launch()
            if (viewModel.launches() % REVIEWS_EACH == 0L) {
                Timber.d("Requesting review")
                val manager = ReviewManagerFactory.create(requireContext())
                try {
                    manager.launchReviewFlow(
                        requireActivity(),
                        manager.requestReviewFlow().await()
                    )
                } catch (ce: CancellationException) {
                    throw ce
                } catch (e: Exception) {
                    Timber.w(e)
                }
            }
        }
    }

    private companion object {
        private const val REVIEWS_EACH = 3
    }
}
