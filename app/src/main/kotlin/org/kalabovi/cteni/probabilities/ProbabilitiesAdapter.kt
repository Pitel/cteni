package org.kalabovi.cteni.probabilities

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.google.android.material.slider.Slider
import org.kalabovi.cteni.databinding.LetterProbabilityBinding
import org.kalabovi.cteni.repo.db.Letter

class ProbabilitiesAdapter(
    val onProbabilityChanged: (letter: String, probability: Float) -> Unit,
    val onEnabledChanged: (letter: String, enabled: Boolean) -> Unit
) :
    ListAdapter<Letter, ProbabilitiesAdapter.ProbabilitiesViewHolder>(DIFF) {
    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int) = getItem(position).letter.hashCode().toLong()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ProbabilitiesViewHolder(
        LetterProbabilityBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ProbabilitiesViewHolder, position: Int) {
        val letter = getItem(position)
        holder.binding.letter.text = letter.letter.uppercase()
        holder.binding.probability.apply {
            clearOnSliderTouchListeners()
            value = letter.probability
            addOnSliderTouchListener(object : Slider.OnSliderTouchListener {
                override fun onStartTrackingTouch(slider: Slider) {}

                override fun onStopTrackingTouch(slider: Slider) {
                    onProbabilityChanged(letter.letter, slider.value)
                }
            })
        }
        holder.binding.enabled.apply {
            clearOnCheckedStateChangedListeners()
            isChecked = letter.enabled
            addOnCheckedStateChangedListener { checkBox, _ ->
                onEnabledChanged(letter.letter, checkBox.isChecked)
            }
        }
    }

    inner class ProbabilitiesViewHolder(val binding: LetterProbabilityBinding) :
        ViewHolder(binding.root)

    private companion object {
        private val DIFF = object : DiffUtil.ItemCallback<Letter>() {
            override fun areItemsTheSame(oldItem: Letter, newItem: Letter) =
                oldItem.letter == newItem.letter

            override fun areContentsTheSame(oldItem: Letter, newItem: Letter) =
                oldItem == newItem
        }
    }
}