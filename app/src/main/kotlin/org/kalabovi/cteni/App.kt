package org.kalabovi.cteni

import android.app.Application
import android.net.TrafficStats
import android.os.StrictMode
import androidx.core.graphics.Insets
import androidx.fragment.app.FragmentManager
import androidx.room.Room
import com.google.android.material.color.DynamicColors
import kotlinx.coroutines.flow.MutableStateFlow
import org.kalabovi.cteni.probabilities.ProbabilitiesViewModel
import org.kalabovi.cteni.reading.ReadingViewModel
import org.kalabovi.cteni.repo.DrillRepository
import org.kalabovi.cteni.repo.ReviewRepository
import org.kalabovi.cteni.repo.ScoreRepository
import org.kalabovi.cteni.repo.TtsRepository
import org.kalabovi.cteni.repo.WordsRepository
import org.kalabovi.cteni.repo.db.Letter
import org.kalabovi.cteni.repo.db.ReadingDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.viewModelOf
import org.koin.dsl.module
import timber.log.Timber
import java.util.Locale

class App : Application() {
    init {
        TrafficStats.setThreadStatsTag(THREAD_STATS_TAG)
        Timber.plant(CrashlyticsTree())
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build()
            )
            FragmentManager.enableDebugLogging(true)
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.DEBUG else Level.INFO)
            androidContext(this@App)
            properties(mapOf(INSETS_KEY to MutableStateFlow(Insets.NONE)))
            modules(
                module {
                    viewModelOf(::ReadingViewModel)
                    viewModelOf(::ProbabilitiesViewModel)
                    singleOf(::DrillRepository)
                    singleOf(::ScoreRepository)
                    singleOf(::WordsRepository)
                    singleOf(::TtsRepository)
                    singleOf(::ReviewRepository)
                    single {
                        Room.databaseBuilder(
                            androidContext(),
                            ReadingDatabase::class.java,
                            "reading"
                        ).build()
                    }
                }
            )
        }
    }

    companion object {
        private const val THREAD_STATS_TAG = 42
        const val INSETS_KEY = "INSETS"

        val LOCALE: Locale = Locale.forLanguageTag("cs-CZ")
        val LETTERS = arrayOf(
            arrayOf(
                "m",
                "l",
                "s",
                "p",
                "t",
                "j",
                "n",
                "v",
                "z",
                "d",
                "k",
                "š",
                "r",
                "c",
                "h",
                "b",
                "č",
                "ž",
                "ř",
                "ch",
                "f",
                "g",
                "ď",
                "ť",
                "ň",
            ),
            arrayOf(
                "a",
                "á",
                "e",
                "é",
                "ě",
                "o",
                "ó",
                "u",
                "ú",
                "ů",
                "i",
                "í",
                "y",
                "ý"
            )
        ).flatMapIndexed { i, letters ->
            letters.map {
                Letter(it, i, 1f, true)
            }
        }.toTypedArray()
    }
}
