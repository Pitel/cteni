package org.kalabovi.cteni.probabilities

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import org.kalabovi.cteni.repo.db.Letter

class ProbabilitiesViewModelTest : StringSpec({
    val viewModel = ProbabilitiesViewModel(
        mockk {
            every { letterDao() } returns mockk {
                every { getAll() } returns flowOf(
                    LETTERS.reversed().map { Letter(it, 0, 0f, false) }
                )
            }
        },
        mockk(),
        mockk()
    )

    "sort" {
        viewModel.letters.first().map { it.letter } shouldBe LETTERS
    }

    afterTest {
        clearAllMocks()
    }
}) {
    private companion object {
        private val LETTERS = listOf("a", "á", "b", "h", "ch", "i", "u", "ú", "ů")
    }
}
