package org.kalabovi.cteni.reading

import com.google.firebase.ktx.Firebase
import com.google.firebase.perf.ktx.performance
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldBeIn
import io.kotest.matchers.ints.shouldBeGreaterThan
import io.kotest.matchers.shouldBe
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.unmockkAll
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.setMain
import org.kalabovi.cteni.App
import org.kalabovi.cteni.reading.ReadingViewModel.Companion.randomChar
import org.kalabovi.cteni.repo.db.Letter

@OptIn(ExperimentalCoroutinesApi::class)
class ReadingViewModelTest : StringSpec({
    "single" {
        listOf(Letter("x", 0, 0f, false)).randomChar shouldBe "x"
    }

    "absolute" {
        listOf(
            Letter("a", 0, 0f, false),
            Letter("b", 0, 1f, false)
        ).randomChar shouldBe "b"

        listOf(
            Letter("a", 0, 1f, false),
            Letter("b", 0, 0f, false)
        ).randomChar shouldBe "a"
    }

    "probabilities" {
        val letters = listOf(
            Letter("a", 0, 1f, false),
            Letter("b", 0, 0.5f, false)
        )
        val counts = List(100) { letters.randomChar }.groupingBy { it }.eachCount()
        counts.keys shouldBe letters.map { it.letter }.toSet()
        counts["a"]!! shouldBeGreaterThan counts["b"]!!
    }

    "soft e syllables" {
        Dispatchers.setMain(Dispatchers.Default)
        mockkStatic(Firebase::performance)
        every { Firebase.performance } returns mockk(relaxed = true)
        val viewModel = ReadingViewModel(
            mockk(relaxed = true),
            mockk(relaxUnitFun = true) {
                coEvery { isEmpty() } returns true
            },
            mockk {
                every { letterDao() } returns mockk {
                    every { getAll() } returns flowOf(
                        App.LETTERS.filter { it.index == 0 } + Letter("ě", 1, 0f, true)
                    )
                    every { getEnabled() } returns getAll()
                }
            },
            mockk {
                coEvery { enabled() } returns false
            },
            mockk(),
            mockk()
        )
        repeat(100) {
            viewModel.correct()
            viewModel.syllable.value.also { println(it) }.lowercase().shouldBeIn(
                "dě",
                "tě",
                "ně",
                "bě",
                "pě",
                "vě",
                "mě",
            )
        }
        unmockkAll()
    }
})
